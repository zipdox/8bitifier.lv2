CC=gcc
SOURCE=8bitifier.c
OBJECT=8bitifier.o
SHAREDOBJECT=8bitifier.so
OPTS=-Wall -fpic

all: $(SHAREDOBJECT)

$(OBJECT): $(SOURCE)
	gcc -c $(OPTS) -o $@ $< -lm

$(SHAREDOBJECT): $(OBJECT)
	$(CC) -shared -o $@ $<

clean:
	rm *.so
	rm *.o
