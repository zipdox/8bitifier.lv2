# 8Bitifier
LV2 plugin to make something sound "8-bit". Hard resamples to 8 kHz and aliases to 8 bits of resolution.

## Building
Make sure you have the LV2 development library installed. On Debian this is the `lv2-dev` package. Of course you will also need gcc and make. Then simply run `make`.

# Installing
After building, Simply copy the 8bitifier.lv2 directory into one of the directories your plugin host scans.
