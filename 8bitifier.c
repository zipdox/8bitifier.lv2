/*
 *  8Bitifier
 *  Copyright (C) 2022  Zipdox
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <lv2/core/lv2.h>

#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include <stdint.h>

#define BITIFIER_URI "http://plugins.zipdox.net/8bitifier"

typedef enum {
    BITIFIER_INPUT_L,
    BITIFIER_INPUT_R,
    BITIFIER_OUTPUT_L,
    BITIFIER_OUTPUT_R,
    BITIFIER_BITS,
    BITIFIER_DIVIDER
} PortIndex;

typedef struct {
    int smp_i;
    const float* input_l;
    const float* input_r;
    float*       output_l;
    float*       output_r;
    float prev_l;
    float prev_r;
    float* bits;
    float* divider;
} Bitifier;

static LV2_Handle
instantiate(const LV2_Descriptor*     descriptor,
            double                    rate,
            const char*               bundle_path,
            const LV2_Feature* const* features)
{
    Bitifier* bitifier = (Bitifier*)calloc(1, sizeof(Bitifier));

    return (LV2_Handle)bitifier;
}

static void
connect_port(LV2_Handle instance, uint32_t port, void* data)
{
    Bitifier* bitifier = (Bitifier*)instance;

    switch ((PortIndex)port) {
        case BITIFIER_INPUT_L:
            bitifier->input_l = (const float*)data;
            break;
        case BITIFIER_INPUT_R:
            bitifier->input_r = (const float*)data;
            break;
        case BITIFIER_OUTPUT_L:
            bitifier->output_l = (float*)data;
            break;
        case BITIFIER_OUTPUT_R:
            bitifier->output_r = (float*)data;
            break;
        case BITIFIER_BITS:
            bitifier->bits = (float*)data;
            break;
        case BITIFIER_DIVIDER:
            bitifier->divider = (float*)data;
            break;
    }
}

static void
activate(LV2_Handle instance)
{
    Bitifier* bitifier = (Bitifier*)instance;
    bitifier->smp_i = 0;
}

#define HALF24 8388607.0f

static inline float normalize(float value){
    if(value > 1.0f) return 1.0f;
    if(value < -1.0f) return -1.0f;
    return value;
}

static inline float bits_precision(float sample, int bits){
    int32_t smp = sample * HALF24;
    smp &= (0xFFFFFFFF << (24 - bits));
    return smp / HALF24;
}

static void
run(LV2_Handle instance, uint32_t n_samples)
{
    Bitifier* bitifier = (Bitifier*)instance;

    const float* const input_l  = bitifier->input_l;
    const float* const input_r  = bitifier->input_r;
    float* const       output_l = bitifier->output_l;
    float* const       output_r = bitifier->output_r;

    int bits = roundf(*(bitifier->bits));
    int divider = roundf(*(bitifier->divider));

    float sample;

    for (uint32_t pos = 0; pos < n_samples; pos++, bitifier->smp_i++) {
        if(bitifier->smp_i >= divider) bitifier->smp_i = 0;

        if(bitifier->smp_i == 0){
            sample = input_l[pos];
            bitifier->prev_l = bits_precision(sample, bits);

            sample = input_r[pos];
            bitifier->prev_r = bits_precision(sample, bits);
        }
        output_l[pos] = bitifier->prev_l;
        output_r[pos] = bitifier->prev_r;

    }
}

static void
deactivate(LV2_Handle instance)
{}

static void
cleanup(LV2_Handle instance)
{
  free(instance);
}

static const void*
extension_data(const char* uri)
{
  return NULL;
}

static const LV2_Descriptor descriptor = {BITIFIER_URI,
                                          instantiate,
                                          connect_port,
                                          activate,
                                          run,
                                          deactivate,
                                          cleanup,
                                          extension_data};

LV2_SYMBOL_EXPORT
const LV2_Descriptor*
lv2_descriptor(uint32_t index)
{
  return index == 0 ? &descriptor : NULL;
}
